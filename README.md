# CPU_try2
This is my second attempt at building a CPU. This one was successful but with many flaws, so I will make a rewrite soon

![](CPU.svg)

# Try my CPU
- Step 1: install the software 
    [https://github.com/hneemann/Digital]
- Step 2: git clone my repository in any folder you wish
- Step 3: open CPU.dig in the digital software
- Step 4: run ./hello_world_CPU/build.sh
- Step 4: run ./hello_world_CPU/bin/hello_world_in_CPU
