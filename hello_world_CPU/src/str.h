#ifndef STR__H
#define STR__H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct {
	char* str;
	size_t len;
} Str;

#define STR_FROM_CSTRING_ARR(a) (Str) {.str=a, .len=sizeof(a) - 1}

Str str_from_cstring(const char* string);
Str str_split_by_char(Str string, char c, int index);
void str_print(Str str);
Str str_trim(Str str);
int str_compare(Str str1, Str str2, bool ignore_case);
bool str_contains(Str str, Str word, bool ignore_case);
bool str_to_int64(Str str, int64_t* num);
Str str_slice(Str str, uint64_t start, int64_t end);
Str str_split_by_num_chars(Str str, int num_chars, int index);


void wrap_print(Str str);

#endif
