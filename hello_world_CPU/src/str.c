#include "str.h"
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

void wrap_print(Str str) {
	printf("|");
	str_print(str);
	printf("|, length: %ld\n", str.len);
}

Str str_slice(Str str, uint64_t start, int64_t end) {
	assert(str.str);
	if (end == -1) {
		end = str.len;
	}
	assert(end-start >= 0);
	assert(end <= str.len);
	return (Str) {
		.str = str.str + start,
		.len = end-start,
	};
}

int str_compare_with_min(Str str1, Str str2, bool ignore_case, size_t min) {
	assert(str1.str);
	assert(str2.str);
	if (!min) {
		min = (str1.len > str2.len) ? str2.len : str1.len;
	}
	for (int i=0; i<min; i++) {
		char a = (ignore_case) ? tolower(str1.str[i]) : str1.str[i];
		char b = (ignore_case) ? tolower(str2.str[i]) : str2.str[i];
		if (a < b) {
			return -1;
		}else if (a > b) {
			return 1;
		}
	}
	if (min) {
		return 0;
	}
	if (str1.len == str2.len) {
		return 0;
	}else if (str1.len == min) {
		return -1;
	}else {
		return 1;
	}
}

bool str_to_int64(Str str, int64_t* num) {
	assert(str.str);
	assert(num);
	if (!str.len) {
		return false;
	}
	*num = 0;
	int start = 0;
	int end = str.len-1;
	int increment = 1;
	if (str.str[0] == '-') {
		increment = -1;
		start = 1;
	}
	int pow = 1;
	if (!str.str[end]) {
		end--;
	}
	for (int i=end; i>=start; i--) {
		if (str.str[i] >'9' || str.str[i] < '0') {
			printf("%d:%d\n", i, str.str[i]);
			return false;
		}
		*num += increment*pow*(str.str[i]-'0');
		pow*=10;
	}
	return true;
}

Str str_from_cstring(const char* string) {
	assert(string);
	size_t len = 0;
	for (; string[len]; len++);
	return (Str) {
		.str = (char*)string,
		.len = len - 1
	};
}

Str str_split_by_num_chars(Str str, int num_chars, int index) {
	assert(str.str);
	if (index*num_chars > str.len) {
		return (Str) {.str = str.str};
	}
	return (Str) {
		.str = str.str+(index*num_chars),
		.len = num_chars
	};
}

Str str_split_by_char(Str string, char c, int index) {
	assert(string.str);
	int current_count = 0;
	Str split = {
		.len = 0,
		.str = string.str,
	};
	for (int i=0; i<string.len; i++) {
		if (string.str[i] == c) {
			if (index == current_count) {
				return split;
			}
			current_count++;
			if (index == current_count) {
				split.str = string.str+i+1;
				continue;
			}
		}
		if (index == current_count) {
			split.len++;
		}
	}
	return split;
}

Str str_to_lower(Str str) {
	assert(str.str);
	Str new_str = str;
	for (int i=0; i<str.len; i++) {
		new_str.str[i] = tolower(str.str[i]);
	}
	return new_str;
}

Str str_to_upper(Str str) {
	assert(str.str);
	Str new_str = str;
	for (int i=0; i<str.len; i++) {
		new_str.str[i] = toupper(str.str[i]);
	}
	return new_str;
}

Str str_replace_char(Str str, char old, char new) {
	assert(str.str);
	Str new_str = str;
	for (int i=0; i<str.len; i++) {
		if (str.str[i] == old) {
			new_str.str[i] = new;
		}
	}
	return new_str;
}

int str_compare(Str str1, Str str2, bool ignore_case) { 
	return str_compare_with_min(str1, str2, ignore_case, 0);
}

bool str_contains(Str str, Str word, bool ignore_case) {
	assert(str.str);
	assert(word.str);
	if (str.len < word.len) {
		return false;
	}
	for (int i=0; str.len-word.len; i++) {
		if (!str_compare_with_min(str, word, ignore_case, word.len)) {
			return true;
		}
	}
	return false;
}

Str str_trim(Str str) {
	assert(str.str);
	Str new_str = str;
	for (int i=0; i<str.len && isspace(str.str[i]); i++) {
		new_str.str++;
		new_str.len--;
	}
	for (int i=new_str.len-1; i>=0; i--) {
		if (new_str.str[i] && !isspace(new_str.str[i])) {
			break;
		}
		new_str.len--;
	}
	return new_str;
}

void str_print(Str str) {
	if (!str.str) {
		printf("null");
		return;
	}
	for (int i=0; i<str.len; i++) {
		putchar(str.str[i]);
	}
}
