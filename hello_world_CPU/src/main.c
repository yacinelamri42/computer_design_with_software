#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdbool.h>
#include "str.h"


void error(char *msg) {
	perror(msg);
	exit(0);
}
void print_arr(uint16_t* array, size_t array_len) {
	for (int i=0; i<array_len/2; i++) {
		printf("%X ", array[i]);
	}
	printf("\n");
}

void send_data(uint16_t* array, size_t array_len, int portno) {
	print_arr(array, array_len);
	struct sockaddr_in serveraddr;
	struct hostent *server;
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		error("ERROR opening socket");
	}
	char hostname[] = "127.0.0.1";
	server = gethostbyname(hostname);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host as %s\n", hostname);
		exit(0);
	}
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
       (char *)&serveraddr.sin_addr.s_addr, server->h_length);
	serveraddr.sin_port = htons(portno);
	if (connect(sockfd, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) < 0) {
		error("ERROR connecting");
	}
	int n = write(sockfd, array, array_len);
	if (n < 0) {
		error("ERROR writing to socket");
	}
	close(sockfd);
}

typedef struct {
	char* source;
	char* output;
} Args;

void handle_args(int argc, char* argv[], Args* args) {
	if (argc != 2 && argc != 4) {
		fprintf(stderr, "Usage: %s source.asm [-o output]\n", argv[0]);
		exit(1);
	}
	for (int i=1; i<argc; i++) {
		if (argv[i][0] == '-') {
			switch (argv[1][1]) {
				case '0':
					if (i == argc-1) {
						fprintf(stderr, "option %s undefined\n", argv[i]);
						exit(1);
					}
					args->output = argv[i+1];
				break;
				default: 
					fprintf(stderr, "option %s undefined\n", argv[i]);
					exit(1);
				break;
			}
		}else {
			args->source = argv[i];
		}
	}
}


int main(int argc, char* argv[]) {
	// Args args = {0};
	// handle_args(argc, argv, &args);
	// FILE* file = fopen(args.source, "r");
	// if (!file) {
	// 	fprintf(stderr, "Could not open file: %s\n", args.source);
	// 	return 1;
	// }
	// fclose(file);
	uint16_t arr[] = {
		[0x0] = 0,
		[0x1] = 0x202E,
		[0x2] = 0xD,
		[0x3] = 'h',
		[0x4] = 'e',
		[0x5] = 'l',
		[0x6] = 'l',
		[0x7] = 'o',
		[0x8] = ' ',
		[0x9] = 'w',
		[0xA] = 'o',
		[0xB] = 'r',
		[0xC] = 'l',
		[0xD] = 'd',
		[0xE] = 10,
		[0x20] = 0x13, // R0 = 2
		[0x21] = 0x13,
		[0x22] = 0x3013, // R3 = 2
		[0x23] = 0x3013,
		[0x24] = 0x100B, // R1 = 0xC
		[0x25] = 0x100D, // compare 
		[0x26] = 0x2031,
		[0x27] = 0x208E,
		[0x28] = 0x13, // R0++
		[0x29] = 0x500B, // R5 = (R0)
		[0x2A] = 0x5302, // print R5
		[0x2B] = 0x252E, // jmp 0x25
	};
	
	send_data(arr, sizeof(arr), 4440);
	return 0;
}
