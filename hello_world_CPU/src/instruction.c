#include "instruction.h"
#include "str.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

// typedef struct {
// 	InstructionArgsLayout args_required;
// 	const char* instruction_name;
// 	uint8_t opcode;
// } Instruction;

Instruction instructions[] = {
	(Instruction){
		.instruction_name="nop",
		.opcode=0x0,
		.args_required=NO_ARGS,
	},
	(Instruction){
		.instruction_name="movr",
		.opcode=0x01,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="movp",
		.opcode=0x31,
		.args_required=DEST_REG,
	},
	(Instruction){
		.instruction_name="out",
		.opcode=0x02,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="in",
		.opcode=0x12,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="addr",
		.opcode=0x03,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="incr",
		.opcode=0x13,
		.args_required=DEST_REG,
	},
	(Instruction){
		.instruction_name="subr",
		.opcode=0x04,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="decr",
		.opcode=0x14,
		.args_required=DEST_REG,
	},
	(Instruction){
		.instruction_name="andr",
		.opcode=0x05,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="orr",
		.opcode=0x06,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="notr",
		.opcode=0x07,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="xorr",
		.opcode=0x08,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="shiftlr",
		.opcode=0x09,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="shiftrr",
		.opcode=0x0a,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="loadr",
		.opcode=0x0b,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="storer",
		.opcode=0x0c,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="cmpr",
		.opcode=0x0d,
		.args_required=DEST_AND_SRC_REGS,
	},
	(Instruction){
		.instruction_name="jmpr",
		.opcode=0x0e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jmp",
		.opcode=0x2e,
		.args_required=BYTE_IN,
	},
	(Instruction){
		.instruction_name="je",
		.opcode=0x3e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jne",
		.opcode=0x4e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jg",
		.opcode=0x5e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jge",
		.opcode=0x6e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jl",
		.opcode=0x7e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="jle",
		.opcode=0x8e,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="movi",
		.opcode=0x0f,
		.args_required=SRC_REG,
	},
	(Instruction){
		.instruction_name="iret",
		.opcode=0x1f,
		.args_required=NO_ARGS,
	},
	(Instruction){
		.instruction_name="int",
		.opcode=0x2f,
		.args_required=BYTE_IN,
	},
	(Instruction){
		.instruction_name="enable_i",
		.opcode=0x3f,
		.args_required=NO_ARGS,
	},
	(Instruction){
		.instruction_name="disable_i",
		.opcode=0x4f,
		.args_required=NO_ARGS,
	},
};

size_t assemble(FILE* file, uint16_t* output) {
	char buffer[MAX_BUFFER];
	for (int i=0; fgets(buffer, MAX_BUFFER, file); i++) {
		Str line = str_trim(str_from_cstring(buffer));
		Str word = str_split_by_char(line, ' ', 0);
		size_t word_count = 0;
		uint8_t opcode = 0;
		uint8_t args = 0;
		for (int j=1; word.len+word.str<line.str+line.len; j++) {
			if (word.len) {
				switch(++word_count) {
					case 1:
					{
						for (int k=0; k<sizeof(instructions)/sizeof(instructions[0]); k++) {
							if (str_compare(word, str_from_cstring(instructions[k].instruction_name), true)) {
								opcode = instructions[k].opcode;
								break;
							}
						}
					}
					break;
					case 2:
					{
						switch (word.str[0]) {
							case 'r':
							case 'R':
								word.str++;
								int64_t num = 0;
								if(!str_to_int64(word, &num)) {
									
								}
							break;
						}
					}
					break;
					case 3:
					{

					}
					default:
					{
						printf("Error, too many args at line: %d: \n", i);
						str_print(line);
						printf("\n");
						exit(1);
					}
				}
			}
			word = str_split_by_char(line, ' ', j);
		}
	}
}

