#ifndef INSTRUCTION__H
#define INSTRUCTION__H

#include <stdint.h>
#include <stdio.h>


#define MAX_BUFFER 4096
typedef enum {
	NO_ARGS,
	DEST_REG,
	SRC_REG,
	DEST_AND_SRC_REGS,
	BYTE_IN
} InstructionArgsLayout;

typedef struct {
	InstructionArgsLayout args_required;
	const char* instruction_name;
	uint8_t opcode;
} Instruction;

size_t assemble(FILE* file, uint16_t* output);
#endif
